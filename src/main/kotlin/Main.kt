import org.sql2o.Sql2o
import java.util.*
import kotlin.collections.ArrayList
import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    var cfgPath = "./cfg.ini"
    if (args.isNotEmpty())
        cfgPath = args[0]
    val cfg = Config(cfgPath)
    val mimeTypeGetter = MagicMimeTypeGetter()
    val fsContentGetter = DirectoryContentFromFsGetter(cfg, mimeTypeGetter)
    val db = Sql2o(cfg.getDbConnectionStr(), cfg.getDbUsername(), cfg.getDbPassword(), Sql2oQuirks())
    val connectionProvider = Sql2oConnectionProvider(db)
    val directoryInfoRepository = Sql2oDirectoryInfoRepository(connectionProvider)
    val fileInfoRepository = Sql2oFileInfoRepository(connectionProvider)
    val fsChangesHandler = FsChangesHandler(fileInfoRepository, directoryInfoRepository, fsContentGetter)
    val watchService = FsWatchService(cfg, fsChangesHandler)

    lateinit var content: DirectoryContentFromFsGetter.Result
    val elpasedCollecting = measureTimeMillis { content = fsContentGetter.getAll() }

    printResult(content)

    fileInfoRepository.clear()
    directoryInfoRepository.clear()

    val elpasedInserting = measureTimeMillis { insertToDb(fileInfoRepository, directoryInfoRepository, content) }

    println("number of directories: ${content.directories.size}, number of files: ${content.files.size}")
    println("collect data: ${elpasedCollecting}ms")
    println("inserting data: ${elpasedInserting}ms")
    println("collect data: ${elpasedCollecting + elpasedInserting}ms")

    watchService.watch()
}

data class MapToDtoResult(val info: Collection<DirectoryInfo>, val dirs: Map<String, UUID>)

fun mapToDto(fsDirs: Collection<DirectoryContentFromFsGetter.FsDir>): MapToDtoResult {
    val result = ArrayList<DirectoryInfo>(fsDirs.size + 1)
    val dirMap = HashMap<String, UUID>()
    val rootId = UUID.randomUUID()
    dirMap[""] = rootId
    result.add(DirectoryInfo(rootId, "", "", null))
    for (fsDir in fsDirs) {
        var parentId = dirMap.get(fsDir.parentDirPath)
        if (parentId == null) {
            parentId = UUID.randomUUID()
            dirMap[fsDir.parentDirPath] = parentId
        }
        val id = UUID.randomUUID()
        dirMap[fsDir.path] = id
        result.add(DirectoryInfo(id = id, name = fsDir.fname, path = fsDir.path, parentDir = parentId))
    }

    return MapToDtoResult(result, dirMap)
}

fun mapToDto(fsFiles: Collection<DirectoryContentFromFsGetter.FsFile>, dirs: Map<String, UUID>) =
    fsFiles.map { FileInfo(UUID.randomUUID(), it.fname, it.path, it.mimeType, it.size, dirs.getValue(it.parentDirPath)) }

fun insertToDb(
    fileInforRepo: FileInfoRepository,
    directoryInfoRepository: Sql2oDirectoryInfoRepository,
    content: DirectoryContentFromFsGetter.Result
) {
    val mapResult = mapToDto(content.directories)
    directoryInfoRepository.addAll(mapResult.info)
    fileInforRepo.addAll(mapToDto(content.files, mapResult.dirs))
}

fun printResult(content: DirectoryContentFromFsGetter.Result) {
    for (file in content.files) {
        println("${file.fname}\t${file.parentDirPath}\t${file.size}")
    }

    println("-----------------------")

    for (dir in content.directories) {
        println("${dir.path}\t${dir.parentDirPath}")
    }

    println("total files: ${content.files.size}, total directories: ${content.directories.size}")
}
