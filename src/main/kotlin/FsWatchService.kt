import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes

class FsWatchService(private val cfg: Config, private val fsChangesHandler: FsChangesHandler) {
    private val dataDir = cfg.getDataDir()
    private val dataDirPath = Paths.get(dataDir)
    private val watchedDirs = HashMap<WatchKey, Path>()

    fun watch() {
        val root = Paths.get(cfg.getDataDir())
        val watchService = FileSystems.getDefault().newWatchService()

        val dirs = collectDirs(root)

        subscrieToEvents(dirs, watchService)

        watchForever(watchService)
    }

    private fun watchForever(watchService: WatchService) {
        while (true) {
            val watchKey = watchService.take()
            val dir = watchedDirs.getValue(watchKey)
            for (event in watchKey.pollEvents()) {
                event as WatchEvent<Path>
                println("${toRelativePath(event.context())} ${event.kind().name()}")

                handleEvent(dir, event, watchService)

                if (!watchKey.reset()) {
                    val dirPath = watchedDirs.getValue(watchKey)
                    fsChangesHandler.directoryRemoved(dirPath)
                    watchedDirs.remove(watchKey)
                }

                println("watcheddirs: ${watchedDirs.size}")
            }
        }
    }

    private fun handleEvent(dir: Path, event: WatchEvent<Path>, watchService: WatchService) {
        val path = toRelativePath(dir.resolve(event.context()))
        when (event.kind()) {
            StandardWatchEventKinds.ENTRY_CREATE -> {
                if (Files.isDirectory(path)) {
                    subscribeToEvents(path, watchService)
                    fsChangesHandler.directoryAdded(path)
                } else {
                    fsChangesHandler.fileAdded(path, dir)
                }
            }
            StandardWatchEventKinds.ENTRY_DELETE -> {
                if (Files.isDirectory(path))
                    return
                fsChangesHandler.fileRemoved(path)
            }
        }
    }

    private fun collectDirs(dir: Path): Collection<Path> {
        val dirs = ArrayList<Path>()
        val walker = object : SimpleFileVisitor<Path>() {
            override fun preVisitDirectory(p0: Path, p1: BasicFileAttributes?): FileVisitResult {
                dirs.add(p0)
                return super.preVisitDirectory(p0, p1)
            }
        }

        Files.walkFileTree(dir, walker)

        return dirs
    }

    private fun subscrieToEvents(dirs: Collection<Path>, watchService: WatchService) =
            dirs.forEach { subscribeToEvents(it, watchService) }

    private fun subscribeToEvents(dir: Path, watchService: WatchService) {
        val key = dir.register(watchService,
                StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_MODIFY,
                StandardWatchEventKinds.ENTRY_DELETE)
        watchedDirs[key] = toRelativePath(dir)
    }

    private fun toRelativePath(path: Path): Path {
        if (!path.isAbsolute)
            return path
        return dataDirPath.relativize(path)
    }
}