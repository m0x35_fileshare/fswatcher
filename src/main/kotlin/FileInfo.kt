import java.util.UUID as UUID

data class FileInfo (
    var id: UUID? = null,
    var name: String = "",
    var path: String = "",
    var mimeType: String = "",
    var size: Long = 0,
    var dir: UUID? = null
)