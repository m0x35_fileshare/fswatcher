import java.util.Optional

interface DirectoryInfoRepository {
    fun findByPath(path: String): Optional<DirectoryInfo>
    fun addAll(items: Collection<DirectoryInfo>)
    fun add(entry: DirectoryInfo)
    fun removeByPath(directoryPath: String)
    fun clear()
}