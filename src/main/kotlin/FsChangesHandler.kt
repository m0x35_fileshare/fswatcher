import java.nio.file.NoSuchFileException
import java.nio.file.Path

class FsChangesHandler(private val fileInfoRepository: FileInfoRepository,
                       private val directoryInfoRepository: Sql2oDirectoryInfoRepository,
                       private val fsContentGetter: DirectoryContentFromFsGetter) {
    fun directoryAdded(dir: Path) {
        val fsDir = fsContentGetter.getDirInfo(dir)
        val dirInfo = DirectoryInfo(name = fsDir.fname, path = fsDir.path)
        val parentDir = directoryInfoRepository.findByPath(fsDir.parentDirPath)
        if (!parentDir.isPresent)
            throw Exception()
        dirInfo.parentDir = parentDir.get().id
        directoryInfoRepository.add(dirInfo)
    }

    fun fileAdded(file: Path, parentDir: Path) {
        val dir = directoryInfoRepository.findByPath(parentDir.toString())
        try {
            val fsFile = fsContentGetter.getFileInfo(file)
            val fileInfo = FileInfo(name = fsFile.fname, path = fsFile.path, dir = dir.get().id, size = fsFile.size)
            fileInfoRepository.add(fileInfo)
        } catch (e: NoSuchFileException) {
            println("File was deleted before it can be queried.")
            e.printStackTrace()
        }
    }

    fun directoryRemoved(dir: Path) {
        directoryInfoRepository.removeByPath(dir.toString())
    }

    fun fileRemoved(file: Path) {
        fileInfoRepository.removeByPath(file.toString())
    }
}