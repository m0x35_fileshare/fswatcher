import java.util.Optional

interface FileInfoRepository {
    fun findByFilePath(filePath: String): Optional<FileInfo>
    fun addAll(entries: Collection<FileInfo>): Collection<FileInfo>
    fun add(entry: FileInfo)
    fun removeByPath(filePath: String)
    fun clear()
}