import java.nio.file.Files;
import java.nio.file.Path;

class MagicMimeTypeGetter: MimeTypeGetter {
    override fun fileMimeType(file: Path): String {
        return Files.probeContentType(file) ?: "application/octet-stream"
    }
}