import java.util.*

class Sql2oFileInfoRepository(private val connectionProvider: Sql2oConnectionProvider) : FileInfoRepository {
    private val table = "file_info"
    private val columnMappings = mapOf(
            "id" to "id",
            "file_path" to "path",
            "file_name" to "name",
            "mime_type" to "mimeType",
            "dir_id" to "dir",
            "file_size" to "size"
    )

    override fun findByFilePath(filePath: String): Optional<FileInfo> {
        val conn = connectionProvider.getConnection()

        val sql = "select * from $table " +
                "where file_path=:filePath;"

        val query = conn
                .createQuery(sql)
                .addParameter("filePath", filePath)
                .setColumnMappings(columnMappings)

        val firstItem = query
                .executeAndFetchFirst(FileInfo::class.java)

        conn.close()

        return if (firstItem != null) Optional.of(firstItem) else Optional.empty()
    }

    override fun add(entry: FileInfo) {
        val conn = connectionProvider.getTransactionConnection()
        val sql = "insert into $table values(:id, :path, :name, :mimeType, :dir, :size);"
        val query = conn.createQuery(sql).setColumnMappings(columnMappings)

        if (entry.id == null)
            entry.id = UUID.randomUUID()

        query.bind(entry)
                .executeUpdate()
        conn.commit()
    }


    override fun addAll(entries: Collection<FileInfo>): Collection<FileInfo> {
        val conn = connectionProvider.getTransactionConnection()
        val sql = "insert into $table values(:id, :path, :name, :mimeType, :dir, :size);"
        val query = conn.createQuery(sql).setColumnMappings(columnMappings)

        for (entry in entries) {
            if (entry.id == null)
                entry.id = UUID.randomUUID()

            query.bind(entry)
                    .addToBatch()
        }

        query.executeBatch()
        conn.commit()

        return entries
    }

    override fun removeByPath(filePath: String) {
        val conn = connectionProvider.getTransactionConnection()
        val sql = "delete from $table where file_path=:path"
        conn.createQuery(sql)
            .addParameter("path", filePath)
            .executeUpdate()

        conn.commit()
    }

    override fun clear() {
        val conn = connectionProvider.getTransactionConnection()
        val sql = "truncate table $table;"
        conn.createQuery(sql).executeUpdate()
        conn.commit()
    }
}