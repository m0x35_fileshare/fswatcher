import org.sql2o.quirks.NoQuirks
import java.sql.SQLException
import java.util.UUID
import java.sql.PreparedStatement



class Sql2oQuirks: NoQuirks(mapOf(UUID::class.java to Sql2oUUIDConverter())) {
    @Throws(SQLException::class)
    override fun setParameter(statement: PreparedStatement, paramIdx: Int, value: UUID) {
        val dbValue = converterOf(UUID::class.java).toDatabaseParam(value)
        statement.setObject(paramIdx, dbValue)
    }

}