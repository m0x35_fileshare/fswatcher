import java.util.*

data class DirectoryInfo (
    var id: UUID? = null,
    var name: String = "",
    var path: String = "",
    var parentDir: UUID? = null
)