import org.sql2o.Connection
import org.sql2o.Sql2o

class Sql2oConnectionProvider(private val db: Sql2o){
    fun getConnection(): Connection = db.open()
    fun getTransactionConnection(): Connection = db.beginTransaction()
}