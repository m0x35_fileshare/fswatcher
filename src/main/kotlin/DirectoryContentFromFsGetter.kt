import java.io.File
import java.io.IOException
import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes

class DirectoryContentFromFsGetter(cfg: Config, private val mimeTypeGetter: MimeTypeGetter) {

    private val dataDir = cfg.getDataDir()
    private val dataDirPath = Paths.get(dataDir)

    data class FsItem(
        val fname: String,
        val path: String,
        val isDirectory: Boolean,
        val size: Long
    )

    data class FsFile(
        val fname: String,
        val path: String,
        val mimeType: String,
        val size: Long,
        val parentDirPath: String
    )

    data class FsDir(
        val fname: String,
        val path: String,
        val parentDirPath: String
    )

    data class Result(
        val directories: Collection<FsDir>,
        val files: Collection<FsFile>
    )

    fun getFileInfo(path: Path) = FsFile(
        fname = path.fileName.toString(),
        path = toRelativePath(path),
        mimeType = getMimeType(dataDirPath.resolve(path)),
        parentDirPath = toRelativePath(path.parent),
        size = Files.size(dataDirPath.resolve(path))
    )

    fun getDirInfo(path: Path) = FsDir(
        fname = path.fileName.toString(),
        path = toRelativePath(path),
        parentDirPath = toRelativePath(path.parent)
    )

    fun get(rawDirPath: String): Collection<FsItem> {
        if (rawDirPath.isEmpty())
            return getRoot()
        val directoryPath = Paths.get(rawDirPath).normalize()
        return getInternal(directoryPath)
    }

    fun getRoot(): Collection<FsItem> {
        return getInternal(Paths.get(""))
    }

    fun getAll(): Result {
        val directoryPath = Paths.get(dataDir)
        val dir = directoryPath.toFile()

        if (!dir.isDirectory)
            throw IllegalArgumentException("Provided argument is not a directory")

        val fsCollector = object : FileVisitor<Path> {

            val directories = ArrayList<FsDir>()
            val files = ArrayList<FsFile>()

            override fun visitFileFailed(path: Path, exception: IOException): FileVisitResult {
                exception.printStackTrace()
                return FileVisitResult.CONTINUE
            }

            override fun preVisitDirectory(path: Path, attributes: BasicFileAttributes): FileVisitResult {
                val parentDirPath = if (path != dataDirPath) toRelativePath(path.parent) else ""
                val dir = FsDir(path.fileName.toString(), toRelativePath(path), parentDirPath)
                if (path != dataDirPath)
                    directories.add(dir)
                return FileVisitResult.CONTINUE
            }

            override fun postVisitDirectory(path: Path, p1: IOException?): FileVisitResult {
                return FileVisitResult.CONTINUE
            }

            override fun visitFile(path: Path, attributes: BasicFileAttributes): FileVisitResult {

                if (attributes.isDirectory)
                    return FileVisitResult.CONTINUE

                val fsItem = FsFile(
                    path.fileName.toString(),
                    toRelativePath(path),
                    getMimeType(path),
                    attributes.size(),
                    toRelativePath(path.parent)
                )
                files.add(fsItem)

                return FileVisitResult.CONTINUE
            }
        }
        Files.walkFileTree(dataDirPath, fsCollector)

        return Result(fsCollector.directories, fsCollector.files)
    }

    private fun getInternal(directoryRelativePath: Path): ArrayList<FsItem> {
        val directoryPath = Paths.get(dataDir, directoryRelativePath.toString())
        val dir = directoryPath.toFile()

        if (!dir.isDirectory)
            throw IllegalArgumentException("Provided argument is not a directory")

        val dirContent = ArrayList<FsItem>()
        val directoryPathAsString = directoryPath.toString()
        dir.walk().maxDepth(1)
            .filterNot { it.isDirectory && (it.name == "." || it.path == directoryPathAsString) }
            .forEach { dirContent.add(createFsItem(it)) }
        return dirContent
    }

    private fun createFsItem(from: File) = FsItem(
        from.name,
        toRelativePath(from.absolutePath),
        from.isDirectory,
        if (!from.isDirectory) from.length() else 0
    )

    private fun toRelativePath(path: String) = toRelativePath(Paths.get(path))

    private fun toRelativePath(path: Path): String {
        if (!path.isAbsolute)
            return path.toString()
        if (path == dataDirPath)
            return ""
        return dataDirPath.relativize(path).toString()
    }

    private fun getMimeType(path: Path): String {
        return mimeTypeGetter.fileMimeType(path)
    }
}
