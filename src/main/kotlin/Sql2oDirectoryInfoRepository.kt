import java.util.*

class Sql2oDirectoryInfoRepository(private val connectionProvider: Sql2oConnectionProvider) : DirectoryInfoRepository {
    private val table = "directory_info"
    private val columnMappings = mapOf("id" to "id",
                                       "dir_path" to "path",
                                       "dir_name" to "name",
                                       "parent_dir_id" to "parentDir")

    override fun findByPath(path: String): Optional<DirectoryInfo> {
        val conn = connectionProvider.getConnection()
        val sql = "select * from $table where dir_path=:path"
        val entry = conn.createQuery(sql).setColumnMappings(columnMappings)
                .addParameter("path", path)
                .setColumnMappings(columnMappings)
                .executeAndFetchFirst(DirectoryInfo::class.java)

        return if (entry != null) Optional.of(entry) else Optional.empty()
    }

    override fun add(entry: DirectoryInfo) {
        val conn = connectionProvider.getTransactionConnection()

        val sql = "insert into $table(id, dir_path, dir_name, parent_dir_id) values(:id, :name, :path, :parentDir);"

        val query = conn.createQuery(sql).setColumnMappings(columnMappings)

        if (entry.id == null)
            entry.id = UUID.randomUUID()

        query.bind(entry)
                .executeUpdate()

        conn.commit()
    }
    override fun addAll(items: Collection<DirectoryInfo>) {
        val conn = connectionProvider.getTransactionConnection()

        val sql = "insert into $table(id, dir_name, dir_path, parent_dir_id) values(:id, :name, :path, :parentDir);"

        val query = conn.createQuery(sql).setColumnMappings(columnMappings)

        for (entry in items) {
            if (entry.id == null)
                entry.id = UUID.randomUUID()

            query.bind(entry)
                .addToBatch()
        }

        query.executeBatch()
        conn.commit()
    }

    override fun removeByPath(directoryPath: String) {
        val conn = connectionProvider.getTransactionConnection()
        val sql = "delete from $table where dir_path=:path"
        conn.createQuery(sql)
                .addParameter("path", directoryPath)
                .executeUpdate()

        conn.commit()
    }

    override fun clear() {
        val conn = connectionProvider.getTransactionConnection()
        val sql = "truncate table $table;"
        conn.createQuery(sql).executeUpdate()
        conn.commit()
    }
}