import java.nio.file.Path

interface MimeTypeGetter {
    fun fileMimeType(file: Path): String
}