import org.sql2o.converters.Converter
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.UUID

class Sql2oUUIDConverter: Converter<UUID> {
    override fun toDatabaseParam(uuid: UUID): Any {
        val raw = ByteArray(16)
        ByteBuffer.wrap(raw)
            .order(ByteOrder.BIG_ENDIAN)
            .putLong(uuid.mostSignificantBits)
            .putLong(uuid.leastSignificantBits)

        return raw
    }

    override fun convert(value: Any?): UUID? {
        if (value == null)
            return null

        if (value is ByteArray) {
            val bb = ByteBuffer.wrap(value)
                .order(ByteOrder.BIG_ENDIAN)
            val firstLong = bb.long
            val secondLong = bb.long
            return UUID(firstLong, secondLong)
        }

        throw Exception("Can't convert type to uuid")
    }
}