create table file_info(id binary(16) not null primary key,
                       file_path varchar(255) not null,
                       file_name varchar(127) not null,
                       dir_id binary(16) not null,
                       file_size integer not null);

create table directory_info(id binary(16) not null primary key,
                            dir_path varchar(255) not null,
                            dir_name varchar(127) not null,
                            parent_dir_id binary(16));
